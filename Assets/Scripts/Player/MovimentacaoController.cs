﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimentacaoController : MonoBehaviour
{

	public static float speedRun = 3.0f;
	public float speedRunFast = 0.0f;
	public static bool isDead = false;
	public GameController gameController;
	public Animator personagem;
	private bool isJumping = false;
	private Rigidbody2D rigiBody2D;

	// Use this for initialization
	void Start ()
	{
		if (SelecaoPersonagem.personagemSelecionado == "cavaleiro") {
			Destroy (gameController.samurai.gameObject);
		} else if (SelecaoPersonagem.personagemSelecionado == "samurai") {
			Destroy (gameController.cavaleiro.gameObject);
		}
		resetDead ();
		resetJumping ();
		rigiBody2D = GetComponent<Rigidbody2D> ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (!isDead) {
			if (speedRunFast != null && speedRunFast > 0) {
				transform.position += Vector3.right * speedRunFast * Time.deltaTime;
			} else {
				transform.position += Vector3.right * speedRun * Time.deltaTime;	
			}

			if (!isJumping && Input.GetKeyDown (KeyCode.Space)) {
				isJumping = true;
				personagem.SetBool ("pulando", true);
				rigiBody2D.AddForce (new Vector2 (0, 8), ForceMode2D.Impulse);
			}
		} else {
			personagem.SetBool ("morto", true);	

			StartCoroutine(ReloadGame());
		}
	}

	IEnumerator ReloadGame ()
	{
		yield return new WaitForSeconds(4);
		gameController.GameOver ();
		resetDead ();
	}

	void OnCollisionEnter2D (Collision2D coll)
	{
		if (coll.gameObject.tag == "solo" || coll.gameObject.tag == "obstaculo") {
			if (isJumping) {
				resetJumping ();
			}
		}
		if (coll.gameObject.tag == "destruidor") {
			isDead = true;
		}
        
		if (coll.gameObject.tag == "MainCamera") {
			isDead = true;
		}
	}

	private void resetJumping ()
	{
		personagem.SetBool ("pulando", false);
		isJumping = false;
	}

	private void resetDead() {
		isDead = false;
		personagem.SetBool ("morto", false);
	}
}
