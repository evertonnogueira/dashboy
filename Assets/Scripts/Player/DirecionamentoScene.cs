﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class DirecionamentoScene : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {

	public string novaSceneNome;
	private bool direcionamentoAtivo = false;

	// Use this for initialization
	void Start () {
		

	}

	// Update is called once per frame
	void Update () {
		if (MovimentacaoController.isDead && (Input.GetMouseButtonUp (0) || Input.GetKey(KeyCode.Space))) {
            // SceneManager.LoadScene("Level1");	
            Scene cenaAtual = SceneManager.GetActiveScene();
            SceneManager.LoadScene(cenaAtual.name);
        }

        if (Input.GetKey (KeyCode.Escape)) {
			SceneManager.LoadScene("Menu");	
		}
	}

	public void OnPointerEnter(PointerEventData eventData)
	{
		this.direcionamentoAtivo = true;
	}

	public void OnPointerExit(PointerEventData eventData)
	{
		this.direcionamentoAtivo = false;
	}
}
