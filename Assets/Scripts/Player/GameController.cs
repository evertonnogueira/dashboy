﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {

	public Text gameOver;
	public Animator cavaleiro;
	public Animator samurai;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
	}


	public void GameOver() {
		if (gameOver) {
			gameOver.text = "Game Over";
			gameOver.transform.position = MoveCamera.posicaoCamera;
			gameOver.enabled = true;	
		}
        Scene cenaAtual = SceneManager.GetActiveScene();
        SceneManager.LoadScene(cenaAtual.name);
    }

}
