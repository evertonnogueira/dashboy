﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomController : MonoBehaviour {

	public Animator cavaleiro;
	public Animator samurai;

	// Use this for initialization
	void Start () {
		Debug.LogWarning (SelecaoPersonagem.personagemSelecionado);


		if (SelecaoPersonagem.personagemSelecionado == "cavaleiro") {
			Destroy (samurai.gameObject);
		} else if (SelecaoPersonagem.personagemSelecionado == "samurai") {
			Destroy (cavaleiro.gameObject);
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
