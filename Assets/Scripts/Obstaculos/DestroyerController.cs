﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DestroyerController : MonoBehaviour {

    public GameObject portaFechada;
	public string proximaScene;
    public static bool endGame = false;
    public GameController game;

    // Use this for initialization
    void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionEnter2D(Collision2D coll) {
        if (gameObject.tag == "chave"){
            Destroy(gameObject);
            Destroy(portaFechada);
        }else if (gameObject.tag == "endGame") {
			Destroy (coll.gameObject);
            endGame = true;

			if (proximaScene != null && proximaScene != "") {
				StartCoroutine(NextLevel());
            }
            else
            {
                game.gameOver.text = "YOU WIN";
            }

		}	
	}

	IEnumerator NextLevel ()
	{
		yield return new WaitForSeconds(3);
		endGame = false;
		SceneManager.LoadScene (proximaScene);

	}
}
