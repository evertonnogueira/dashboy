﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class SelecaoPersonagem : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {

	public Animator maquinaEstado;
	public string selecionado;
	public static string personagemSelecionado;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

	}

	public void OnPointerEnter(PointerEventData eventData)
	{
		maquinaEstado.SetBool (selecionado, true);
	}

	public void OnPointerExit(PointerEventData eventData)
	{
		maquinaEstado.SetBool (selecionado, false);
	}

	public void PersonagemSelecionado() {
		personagemSelecionado = gameObject.tag;
		SceneManager.LoadScene("Level1");
	}

}
