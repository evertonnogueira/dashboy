﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCamera : MonoBehaviour {

    public float diferenca;

	public static Vector3 posicaoCamera;
    // Use this for initialization
    void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
        if (!MovimentacaoController.isDead && !DestroyerController.endGame){
            transform.Translate(Vector3.right * Time.deltaTime * (MovimentacaoController.speedRun - diferenca));
			posicaoCamera = transform.position;
        }
    }
		
}
